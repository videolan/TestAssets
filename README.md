# TestAssets

This directory serves as a storage repo for test video assets.
Right now, the videos are used in [VLCKit's](https://code.videolan.org/videolan/VLCKit) testsuite.

## Directory

- `standard` contains 4 videos of different file formats that are 3-4 seconds long
- `browser` and `raspberry` each contain slightly longer videos with 4 subtitles of different languages

```
.
├── README.md
├── browser
│   ├── browser.mp4
│   ├── chinese-traditional.srt
│   ├── english.srt
│   └── french.srt
├── raspberry
│   ├── german.srt
│   ├── portuguese.srt
│   ├── raspberry.mp4
│   └── slovak.srt
└── standard
    ├── bird.m4v
    ├── bunny.avi
    ├── salmon.mp4
    └── sea_lions.mov
```
